# Core-Systems-80
Stellaris Mod Core Systems (80)

###  *Supported Version: 1.6.** ###
##  Description: ##
Changes Base Core Systems Size to 80 Systems.

## Features: ##
Changes the Core System Size from 3 to 80.

## Incompatibilities: ##
Mods that Change the Base Core System Size.

---

Note: 
Images have not yet been updated to the new System.

#### Support: ####
*https://jira.meridanas.me/servicedesk/customer/portal/5*

#### Direct Download: ####
*https://bitbucket.org/meridanas/core-systems-80/downloads/*